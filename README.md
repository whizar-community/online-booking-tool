# online-booking-tool


**API list**

* search best flight
    - query
    ```
    {
        "origin": "TLV",
        "destination": "JFK",
        "departure_date": "2020-01-01",
        "return_date": "2020-01-15"
    }
    ```
    - response
    ```
    [
        {
            "segments": [
                {
                    "origin": "TLV",
                    "destination": "JFK",
                    "depature_datetime": "2020-01-01T00:44:00",
                    "arrival_datetime": "2020-01-01T04:46:00",
                    "flight_number": "1",
                    "flight_airline": "LY"
                },
                {
                    "origin": "JFK",
                    "destination": "TLV",
                    "depature_datetime": "2020-01-15T16:14:00",
                    "arrival_datetime": "2020-01-16T08:48:00",
                    "flight_number": "2",
                    "flight_airline": "LY"
                }
            ],
            "price": {
                "amount": "750.12",
                "currency": "USD"
            }
        }
    ]
    ```

* create itinerary
    - query
    ```
    {
        "offer": {
            "segments": [
                {
                    "origin": "TLV",
                    "destination": "JFK",
                    "depature_datetime": "2020-01-01T00:44:00",
                    "arrival_datetime": "2020-01-01T04:46:00",
                    "flight_number": "1",
                    "flight_airline": "LY"
                },
                {
                    "origin": "JFK",
                    "destination": "TLV",
                    "depature_datetime": "2020-01-15T16:14:00",
                    "arrival_datetime": "2020-01-16T08:48:00",
                    "flight_number": "2",
                    "flight_airline": "LY"
                }
            ],
            "price": {
                "amount": "750.12",
                "currency": "USD"
            }
        },
        "travelers": [
            {
                "first_name": "John",
                "last_name": "Doe"
            }
        ]
    }
    ```
    - response
    ```
    {
        "status": "success",
        "itinerary_id": "ABCEF"
    }
    ```


* get itinerary
    - query
    ```
    {
        "itinerary_id": "ABCEF"
    }
    ```
    - response
    ```
    {
        "offer": {
            "segments": [
                {
                    "origin": "TLV",
                    "destination": "JFK",
                    "depature_datetime": "2020-01-01T00:44:00",
                    "arrival_datetime": "2020-01-01T04:46:00",
                    "flight_number": "1",
                    "flight_airline": "LY"
                },
                {
                    "origin": "JFK",
                    "destination": "TLV",
                    "depature_datetime": "2020-01-15T16:14:00",
                    "arrival_datetime": "2020-01-16T08:48:00",
                    "flight_number": "2",
                    "flight_airline": "LY"
                }
            ],
            "price": {
                "amount": "750.12",
                "currency": "USD"
            }
        },
        "travelers": [
            {
                "first_name": "John",
                "last_name": "Doe"
            }
        ],
        "tickets": []
    }
    ```


* issue itinerary
    - query
    ```
    {
        "itinerary_id": "ABCEF"
    }
    ```
    - response
    ```
    {
        "status": "success",
        "tickets": ["112233445566"]
    }
    ```

* cancel itinerary
    - query
    ```
    {
        "itinerary_id": "ABCEF"
    }
    ```
    - response
    ```
    {
        "status": "success"
    }
    ```
